# Timetables #

The aim of this project is to build an Android app that can serve Swansea University - College of Science students as a better viewer of their timetables. 
Currently they are offered an option to import their timetables in the iCalendar format to their favourite calendar app, but this has the disadvantage that these calendars cannot filter the sessions the user shouldn't attend. 
This app will try to tackle this by providing a feature where the user can specify certain rules for events to be filtered. Another advantage of this app should be that the sessions will have different colors based on the module they belong to. 
And obviously, this app will alert the user if it detects a lecture/lab session has been canceled.

This project has nearly been finished before the end of second semestre of 2019/2020, the work (which is really just testing and little improvements) will be resumed in September 2020.